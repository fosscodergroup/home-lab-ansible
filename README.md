# Home Lab - Ansible

Ansible repository for my home lab

## Getting started

### Deploy a new VM

The hypervisor in this environment is KVM on Fedora.

To deploy a VM follow these steps.  Note that currently only Fedora 34 server is supported
1. Create a file in inventory/host_vars, which has the FQDN of the VM as the file name and contains the necessary information.
2. Run the [deploy_new_vm.yml](./playbooks/deploy_new_vm.yml) playbook.
NOTE:  You will need a way to feed it the vault password for [kickstart.vault](./vault/kickstart.vault).

### Configuring a new managed node

This environment is configured to use a unique user and SSH key-based authentication for management via Ansible.

To setup a node to be managed, you need to follow these steps.

1. Make sure the new node can be accessed via SSH with password authentication and the remote user can use sudo.
2. Add the IP or FQDN to the [main inventory file](./inventory/inventory.yml).
3. Make sure the user on the ansible control node trusts the fingerprint the IP or FQDN.
4. Run the [configure_new_node.sh](./scripts/configure_new_node.sh) script from the root folder of the repository.