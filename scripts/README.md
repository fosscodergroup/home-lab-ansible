# Home Lab - Ansible

## Scripts Directory

This directory contains one-off scripts.

**NOTE:** Scripts typically need to be run from the root Ansible directory.

This will make sure that ansible.cfg in the root Ansible directory is in scope, which will allow the correct inventory file to be used.