#!/bin/bash

echo "Welcome to the configure a new Ansible managed node script."

ansible_inventory_file="./inventory/inventory.yml"
ansible_playbook_file="./playbooks/configure_new_node.yml"

echo ""
echo "REMINDER: The FQDN you input must already exist in $ansible_inventory_file"
echo "and you must trust this FQDN's fingerprint prior to running this script!"
echo "Press CTRL+C to stop this script and add the FQDN if needed."
echo ""

read -p "Enter the FQDN of the host you want to configure: " remote_hostname
read -p "Enter the user for the remote connection to $remote_hostname: " remote_username

# Do the work

ansible-playbook $ansible_playbook_file -u $remote_username -kK -e "remote_hostname=$remote_hostname remote_username=$remote_username"

# Test for success

echo ""
echo "Testing key-based SSH connection and privilege escalation."

ssh $remote_hostname sudo hostname

echo "If the correct hostname displayed in the previous line and there was no prompt for a password,"
echo "everything was successful!"
