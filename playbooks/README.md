# Home Lab - Ansible

## Playbooks Directory

This directory contains general playbooks.

### host_groups

Playbooks in this directory are intended to be run against groups of hosts, where only minor config variants are needed.

### hosts

Playbooks in this directory are intended to be run against individual hosts.
Files in this directory should follow this naming convention: fqdn.yml