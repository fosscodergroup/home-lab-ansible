---
# VM Configuration

vm_name: home-backup
vm_ram_in_mb: 2048
vm_vcpus: 2
vm_vcpu_sockets: 1
vm_vcpu_cores: 2
vm_os_variant: fedora34
vm_os_is_server: true
vm_disks:
  - disk_size_in_gb: 20
  - disk_size_in_gb: 500

# Network Configuration

netmask: 255.255.255.0
gateway: 192.168.1.1
ipv4_address: 192.168.1.12
dns: 192.168.1.3
hostname: home-backup
...
